# CODE CITATION for Original figures and data tables from _Cities and Citadels_ (2023)

## Particularly essential package callouts go to:-

- sf, stars, raster, terra, hillshader, morphogram, rnaturalearth
- tidyverse
- ggplot2, ggtext, ggpubr, patchwork, ggnewscale, ggrepel, ggfx


## Citation of R packages loaded and used in figure creation code for the book

R Core Team (2023). _R: A Language and Environment for Statistical Computing_. R Foundation for
Statistical Computing, Vienna, Austria. <https://www.R-project.org/>.

Hvitfeldt E (2022). _prismatic: Color Manipulation Tools_. R package version 1.1.1,
<https://CRAN.R-project.org/package=prismatic>.

Izrailev S (2023). _tictoc: Functions for Timing R Scripts, as Well as Implementations of "Stack" and
"StackList" Structures_. R package version 1.2, <https://CRAN.R-project.org/package=tictoc>.

Santos Baquero O (2019). _ggsn: North Symbols and Scale Bars for Maps Created with 'ggplot2' or
'ggmap'_. R package version 0.5.0, <https://CRAN.R-project.org/package=ggsn>.

Pedersen T (2022). _ggfx: Pixel Filters for 'ggplot2' and 'grid'_. R package version 1.0.1,
<https://CRAN.R-project.org/package=ggfx>.

Wilke C, Wiernik B (2022). _ggtext: Improved Text Rendering Support for 'ggplot2'_. R package version
0.1.2, <https://CRAN.R-project.org/package=ggtext>.

FC M, Davis T, ggplot2 authors (2022). _ggpattern: 'ggplot2' Pattern Geoms_. R package version 1.0.1,
<https://CRAN.R-project.org/package=ggpattern>.

Pedersen T (2022). _patchwork: The Composer of Plots_. R package version 1.1.2,
<https://CRAN.R-project.org/package=patchwork>.

Kassambara A (2023). _ggpubr: 'ggplot2' Based Publication Ready Plots_. R package version 0.6.0,
<https://CRAN.R-project.org/package=ggpubr>.

Campitelli E (2023). _ggnewscale: Multiple Fill and Colour Scales in 'ggplot2'_. R package version
0.4.9, <https://CRAN.R-project.org/package=ggnewscale>.

Slowikowski K (2023). _ggrepel: Automatically Position Non-Overlapping Text Labels with 'ggplot2'_. R
package version 0.9.3, <https://CRAN.R-project.org/package=ggrepel>.

Chamberlain S, Barve V, Mcglinn D, Oldoni D, Desmet P, Geffert L, Ram K (2023). _rgbif: Interface to
the Global Biodiversity Information Facility API_. R package version 3.7.7,
<https://CRAN.R-project.org/package=rgbif>.

Chamberlain S, Boettiger C (2017). “R Python, and Ruby clients for GBIF species occurrence data.”
_PeerJ PrePrints_. <https://doi.org/10.7287/peerj.preprints.3304v1>.

Massicotte P, South A (2023). _rnaturalearth: World Map Data from Natural Earth_. R package version
0.3.3, <https://CRAN.R-project.org/package=rnaturalearth>.

Strimas-Mackey M (2023). _smoothr: Smooth and Tidy Spatial Features_. R package version 1.0.1,
<https://CRAN.R-project.org/package=smoothr>.

Roudier P (2021). _hillshader: Create Hillshade Relief Maps Using Ray-Tracing_. R package version
0.1.0, <https://CRAN.R-project.org/package=hillshader>.

Pebesma E, Bivand R (2023). _Spatial Data Science: With applications in R_. Chapman and Hall/CRC,
London. doi:10.1201/9780429459016 <https://doi.org/10.1201/9780429459016>,
<https://r-spatial.org/book/>.

Plate T, Heiberger R (2016). _abind: Combine Multidimensional Arrays_. R package version 1.4-5,
<https://CRAN.R-project.org/package=abind>.

Hijmans R (2023). _terra: Spatial Data Analysis_. R package version 1.7-29,
<https://CRAN.R-project.org/package=terra>.

Hijmans R (2023). _raster: Geographic Data Analysis and Modeling_. R package version 3.6-20,
<https://CRAN.R-project.org/package=raster>.

Pebesma EJ, Bivand RS (2005). “Classes and methods for spatial data in R.” _R News_, *5*(2), 9-13.
<https://CRAN.R-project.org/doc/Rnews/>.

Bivand RS, Pebesma E, Gomez-Rubio V (2013). _Applied spatial data analysis with R, Second edition_.
Springer, NY. <https://asdar-book.org/>.

Pebesma E (2023). _lwgeom: Bindings to Selected 'liblwgeom' Functions for Simple Features_. R package
version 0.2-13, <https://CRAN.R-project.org/package=lwgeom>.

Pebesma E (2018). “Simple Features for R: Standardized Support for Spatial Vector Data.” _The R
Journal_, *10*(1), 439-446. doi:10.32614/RJ-2018-009 <https://doi.org/10.32614/RJ-2018-009>,
<https://doi.org/10.32614/RJ-2018-009>.

Pebesma E, Bivand R (2023). _Spatial Data Science: With applications in R_. Chapman and Hall/CRC.
<https://r-spatial.org/book/>.

Grolemund G, Wickham H (2011). “Dates and Times Made Easy with lubridate.” _Journal of Statistical
Software_, *40*(3), 1-25. <https://www.jstatsoft.org/v40/i03/>.

Posit team (2023). _RStudio: Integrated Development Environment for R_. Posit Software, PBC, Boston,
MA. <http://www.posit.co/>.

Wickham H (2023). _forcats: Tools for Working with Categorical Variables (Factors)_. R package version
1.0.0, <https://CRAN.R-project.org/package=forcats>.

Wickham H (2022). _stringr: Simple, Consistent Wrappers for Common String Operations_. R package
version 1.5.0, <https://CRAN.R-project.org/package=stringr>.

Wickham H, François R, Henry L, Müller K, Vaughan D (2023). _dplyr: A Grammar of Data Manipulation_. R
package version 1.1.2, <https://CRAN.R-project.org/package=dplyr>.

Wickham H, Henry L (2023). _purrr: Functional Programming Tools_. R package version 1.0.1,
<https://CRAN.R-project.org/package=purrr>.

Wickham H, Hester J, Bryan J (2023). _readr: Read Rectangular Text Data_. R package version 2.1.4,
<https://CRAN.R-project.org/package=readr>.

Wickham H, Vaughan D, Girlich M (2023). _tidyr: Tidy Messy Data_. R package version 1.3.0,
<https://CRAN.R-project.org/package=tidyr>.

Müller K, Wickham H (2023). _tibble: Simple Data Frames_. R package version 3.2.1,
<https://CRAN.R-project.org/package=tibble>.

Wickham H (2016). _ggplot2: Elegant Graphics for Data Analysis_. Springer-Verlag New York. ISBN
978-3-319-24277-4, <https://ggplot2.tidyverse.org>.

Wickham H, Averick M, Bryan J, Chang W, McGowan LD, François R, Grolemund G, Hayes A, Henry L, Hester
J, Kuhn M, Pedersen TL, Miller E, Bache SM, Müller K, Ooms J, Robinson D, Seidel DP, Spinu V,
Takahashi K, Vaughan D, Wilke C, Woo K, Yutani H (2019). “Welcome to the tidyverse.” _Journal of Open
Source Software_, *4*(43), 1686. doi:10.21105/joss.01686 <https://doi.org/10.21105/joss.01686>.

Wilkinson T (2023). _morphogram: Morphogram Tableaux to Compare Spatial Features Visually_.
https://tcwilkinson.github.io/morphogram, https://github.com/tcwilkinson/morphogram.
