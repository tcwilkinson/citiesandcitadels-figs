
# Original figures and data tables from _Cities and Citadels_ (2024)

![Cities and Citadels book cover](https://images.tandf.co.uk/common/jackets/crclarge/978100318/9781003183563.jpg)

Contained in this repository are data and finished figures used in the 2024
"Cities and Citidels" book, published by Routledge (with Open Access
publication of the contents supported by the University of York).

The original figures here are released under a CC-BY license. Please cite the
original book as primary source:-

> Green, A.S., Wilkinson, T.C., Wilkinson, D., Highcock, N., & Leppard, T. (2024). Cities and Citadels: An Archaeology of Inequality and Economic Growth (1st ed.). Routledge. https://doi.org/10.4324/9781003183563

You should also cite the following associated website as the place to download
data, in preference to the repository (whose address may change in the future):-

> https://www.citiesandcitadels.org/


## Original figures (high-resolution PDFs) with maps and graphs

- ![](thumbs/fig1.1_ngram.png) — [`fig1.1_ngram.pdf`](pdfs/fig1.1_ngram.pdf)
- ![](thumbs/fig1.2_KEYED_regions_map.png) — [`fig1.2_KEYED_regions_map.pdf`](pdfs/fig1.2_KEYED_regions_map.pdf)
- ![](thumbs/fig2.1_KEYED_city_map.png) — [`fig2.1_KEYED_city_map.pdf`](pdfs/fig2.1_KEYED_city_map.pdf)
- ![](thumbs/fig2.2_city_tableau.png) — [`fig2.2_city_tableau.pdf`](pdfs/fig2.2_city_tableau.pdf)
- ![](thumbs/fig3.1_KEYED_citadel_map.png) — [`fig3.1_KEYED_citadel_map.pdf`](pdfs/fig3.1_KEYED_citadel_map.pdf)
- ![](thumbs/fig3.2_citadel_tableau.png) — [`fig3.2_citadel_tableau.pdf`](pdfs/fig3.2_citadel_tableau.pdf)
- ![](thumbs/fig3.3_KEYED_city_sizing_violin_plots.png) —  [`fig3.3_KEYED_city_sizing_violin_plots.pdf`](pdfs/fig3.3_KEYED_city_sizing_violin_plots.pdf)
- ![](thumbs/fig3.10_citycitadel_tableau.png) — [`fig3.10_citycitadel_tableau.pdf`](pdfs/fig3.10_citycitadel_tableau.pdf)
- ![](thumbs/fig4.1_KEYED_tokens_map.png) — [`fig4.1_KEYED_tokens_map.pdf`](pdfs/fig4.1_KEYED_tokens_map.pdf)
- ![](thumbs/fig4.2_KEYED_weights_map.png) — [`fig4.2_KEYED_weights_map.pdf`](pdfs/fig4.2_KEYED_weights_map.pdf)
- ![](thumbs/fig4.3_KEYED_weight_distribution.png) — [`fig4.3_KEYED_weight_distribution.pdf`](pdfs/fig4.3_KEYED_weight_distribution.pdf)
- ![](thumbs/fig5.1_KEYED_merchants_urIII_map_withinset.png) — [`fig5.1_KEYED_merchants_urIII_map_withinset.pdf`](pdfs/fig5.1_KEYED_merchants_urIII_map_withinset.pdf)
- ![](thumbs/fig5.2_KEYED_merchants_oldassyrian_map.png) — [`fig5.2_KEYED_merchants_oldassyrian_map.pdf`](pdfs/fig5.2_KEYED_merchants_oldassyrian_map.pdf)
- ![](thumbs/fig5.4_KEYED_merchants_ugaritic_map.png) — [`fig5.4_KEYED_merchants_ugaritic_map.pdf`](pdfs/fig5.4_KEYED_merchants_ugaritic_map.pdf)
- ![](thumbs/fig6.1_KEYED_coinage_map.png) — [`fig6.1_KEYED_coinage_map.pdf`](pdfs/fig6.1_KEYED_coinage_map.pdf)
- ![](thumbs/fig6.2_KEYED_wealth_ratios.png) — [`fig6.2_KEYED_wealth_ratios.pdf`](pdfs/fig6.2_KEYED_wealth_ratios.pdf)
- ![](thumbs/fig7.1_r_gt_g_grid_transitions.png) — [`fig7.1_r_gt_g_grid_transitions.pdf`](pdfs/fig7.1_r_gt_g_grid_transitions.pdf)
- ![](thumbs/fig7.2_types_of_economies.png) — [`fig7.2_types_of_economies.pdf`](pdfs/fig7.2_types_of_economies.pdf)


## Keys to labelled maps and graphs (text files)

- [`fig1.2_KEYS.txt`](keys_to_labels/fig1.2_KEYS.txt)
- [`fig2.1_KEYS.txt`](keys_to_labels/fig2.1_KEYS.txt)
- [`fig3.1_KEYS.txt`](keys_to_labels/fig3.1_KEYS.txt)
- [`fig3.3_KEYS.txt`](keys_to_labels/fig3.3_KEYS.txt)
- [`fig4.1_KEYS.txt`](keys_to_labels/fig4.1_KEYS.txt)
- [`fig4.2_KEYS.txt`](keys_to_labels/fig4.2_KEYS.txt)
- [`fig4.3_KEYS.txt`](keys_to_labels/fig4.3_KEYS.txt)
- [`fig5.1_KEYS.txt`](keys_to_labels/fig5.1_KEYS.txt)
- [`fig5.2_KEYS.txt`](keys_to_labels/fig5.2_KEYS.txt)
- [`fig5.4_KEYS.txt`](keys_to_labels/fig5.4_KEYS.txt)
- [`fig6.1_KEYS.txt`](keys_to_labels/fig6.1_KEYS.txt)
- [`fig6.2_KEYS.txt`](keys_to_labels/fig6.2_KEYS.txt)


## Source data

Data files used to generate figures, graphs and insights in text are downloadable
here as CSV or GeoJSON files. The data presented is necessarily an ongoing
work in progress, and does not claim comprehensiveness. It represents the
base spatial/other data needed to create illustrative figures and draw insights
for the book.

- Chapter 1 Pasts
  - [`archaeological_regions.geojson`](data/archaeological_regions.geojson)
- Chapters 2 Cities and 3 Citadels
  - [`city_citadel_areas.geojson`](data/city_citadel_areas.geojson)
  - [`citadel_zones.geojson`](data/citadel_zones.geojson)
  - [`city_citadel_db.csv`](data/city_citadel_db.csv)
  - [`city_citadel_linefeatures.geojson`](data/city_citadel_linefeatures.geojson)
  - [`city_zones.geojson`](data/city_zones.geojson)
- Chapter 4 Measurement
  - [`token_zones.geojson`](data/token_zones.geojson)
  - [`tokens.csv`](data/tokens.csv) - primarily based on distribution listed in Bennison-Chapman, L. E. 2019. ‘Reconsidering “Tokens”: The Neolithic Origins of Accounting or Multifunctional, Utilitarian Tools?’ _Cambridge Archaeological Journal_ 29 (2): 233–59. https://doi.org/10.1017/S0959774318000513.
  - [`weighing_zones.geojson`](data/weighing_zones.geojson)
  - ancient weights - [`petrie-1926.csv`](data/petrie-1926.csv) - extracted catalogued items from Petrie, W.M.F. (1926). _Ancient Weights and Measures_. London: Department of Egyptology: University College. (Note that mass of the weights are given in grains not grams).
- Chapter 5 Merchants
  - [`merchant_routes.geojson`](data/merchant_routes.geojson)
  - [`merchant_sites.csv`](data/merchant_sites.csv)
- Chapter 6 Billionaires
  - [`agrarian_wealth.csv`](data/agrarian_wealth.csv)
  - [`nonagrarian_wealth.csv`](data/nonagrarian_wealth.csv)
  - [`coinage_places.csv`](data/coinage_places.csv)
  - [`coinage_zones_100BC.geojson`](data/coinage_zones_100BC.geojson)


### Linked data sources

The contents of the following data sets are linked here as they are available
in digital form, from the original source publication or associated digital
repository:

- Chapter 4
  - ancient weights - `ialongo-etal2021-pnas-s1.csv`: [See: Ialongo et al. 2021, Dataset S1](https://www.pnas.org/doi/10.1073/pnas.2105873118#data-availability)
  > Ialongo, N., R. Hermann, and L. Rahmstorf. 2021. ‘Bronze Age Weight Systems as a Measure of Market Integration in Western Eurasia’. Proceedings of the National Academy of Sciences 118 (27): 1–9. https://doi.org/10.1073/pnas.2105873118.

- Map Base Data
  - `GDEM_15arcsec_Tozer/SRTM15+V2_landonly_nosmallislands.tif` - Custom edited version of the SRTM digital elevation data, used for the world and super-regional maps.  
  > Tozer, B, Sandwell, D. T., Smith, W. H. F., Olson, C., Beale, J. R., & Wessel, P. (2019). Global bathymetry and topography at 15 arc sec: SRTM15+. _Earth and Space Science_, 6, 1847–1864. https://doi.org/10.1029/2019EA000658

  - `mrb_rivers.json` - Major river basins data, based on the GRDC's version of HydroSHEDS river basins dataset, used for the world and super-regional maps.
  > GRDC (2020): GRDC Major River Basins. Global Runoff Data Centre. 2nd, rev. ed. Koblenz: Federal Institute of Hydrology (BfG).  https://www.bafg.de/GRDC/EN/02_srvcs/22_gslrs/221_MRB/riverbasins_node.html
  >
  > This product “Major River Basins of the World” incorporates data from the HydroSHEDS database which is © World Wildlife Fund, Inc. (2006-2013) and has been used herein under license. WWF has not evaluated the data as altered and incorporated within “Major River Basins of the World”, and therefore gives no warranty regarding its accuracy, completeness, currency or suitability for any particular purpose. Portions of the HydroSHEDS database incorporate data which are the intellectual property rights of © USGS (2006-2008), NASA (2000-2005), ESRI (1992-1998), CIAT (2004-2006), UNEP-WCMC (1993), WWF (2004), Commonwealth of Australia (2007), and Her Royal Majesty and the British Crown and are used under license. The HydroSHEDS database and more information are available at http://www.hydrosheds.org.


## That world map projection

The world maps are cropped segments from Peirce Quincuncial's projection. You can
replicate this map using any GIS or spatial visualization software that supports
a version of the open source `prøj` map projection library *version 9.0* or greater:-

- https://proj.org/en/9.2/operations/projections/peirce_q.html

An interesting property of the full PQ projection is that it can be tesselated.

> Peirce (1879) "A Quincuncial Projection of the Sphere". _American Journal of Mathematics_, 2 (4): 394–397


## Creation code for figures

Most of the maps and graphs were created in R code, using a number of well-known
packages publically available. For a list of packages used to run the code, see
[CODE_CITATION.md](CODE_CITATION.md).


## Citation of the book - date of publication

Please note that while the ebook was released on 19th December 2023, the
reference year of publication of the book is **2024** (as is printed on the
imprint page of both hard-copy and ebook), and hence should be cited
Green et al. 2024 or similar.
